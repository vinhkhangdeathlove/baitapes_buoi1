const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
const renderButton = () => {
  let contentHTML = `
        <button class="color-button ${
          colorList[0]
        } active" onclick="changeColor('${colorList[0]}', '${0}')"></button>
    `;
  for (let i = 1; i < colorList.length; i++) {
    const color = colorList[i];
    contentHTML += `
          <button class="color-button ${color}" onclick="changeColor('${color}', '${i}')"></button>
      `;
  }
  document.getElementById("colorContainer").innerHTML = contentHTML;
};

renderButton();

const changeColor = (color, index) => {
  document.getElementById("house").className = `house ${color}`;
  for (let i = 0; i < colorList.length; i++) {
    document.getElementsByClassName("color-button")[i].className = document
      .getElementsByClassName("color-button")
      [i].className.replace(/ active/g, "");
  }
  document.getElementsByClassName("color-button")[index].className += " active";
};
