const tinhTrungBinh = (...rest) => {
  let total = 0;
  for (let i = 0; i < rest.length; i++) {
    total += rest[i];
  }
  return total / rest.length;
};

document.getElementById("btnKhoi1").addEventListener("click", function () {
  let valueToan = document.getElementById("inpToan").value * 1;
  let valueLy = document.getElementById("inpLy").value * 1;
  let valueHoa = document.getElementById("inpHoa").value * 1;
  document.getElementById("tbKhoi1").innerHTML = tinhTrungBinh(
    valueToan,
    valueLy,
    valueHoa
  ).toFixed(2);
});

document.getElementById("btnKhoi2").addEventListener("click", function () {
  let valueVan = document.getElementById("inpVan").value * 1;
  let valueSu = document.getElementById("inpSu").value * 1;
  let valueDia = document.getElementById("inpDia").value * 1;
  let valueEnglish = document.getElementById("inpEnglish").value * 1;
  document.getElementById("tbKhoi2").innerHTML = tinhTrungBinh(
    valueVan,
    valueSu,
    valueDia,
    valueEnglish
  ).toFixed(2);
});
