const jumpText = () => {
  let text = document.getElementsByClassName("heading")[0].innerText;
  let contentHTML = "";
  let chars = [...text];
  for (let i = 0; i < chars.length; i++) {
    contentHTML += `<span>${chars[i]}</span>`;
  }
  document.getElementsByClassName("heading")[0].innerHTML = contentHTML;
};

jumpText();
